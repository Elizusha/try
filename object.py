import cv2
import numpy as np

class Object:
    def __init__(self, name = "Object"):
        self.xPos = -1
        self.yPos = -1
        self.type = name
        if name == "Object":
            self.Color = (0, 0, 0)
            return
        if name == "blue":
            self.HSVmin = np.array((91,93,126), np.uint8)
            self.HSVmax = np.array((108,201,255), np.uint8)
            self.Color = (255,0,0)
        if name == "green":
            self.HSVmin = np.array((40,70,101), np.uint8)
            self.HSVmax = np.array((68,162,255), np.uint8)
            self.Color = (0,255,0)
        if name == "orange":
            self.HSVmin = np.array((23,134,94), np.uint8)
            self.HSVmax = np.array((42,255,255), np.uint8)
            self.Color = (0,255,255)
        if name == "red":
            self.HSVmin = np.array((0,105,101), np.uint8)
            self.HSVmax = np.array((14,218,255), np.uint8)
            self.Color = (0,0,255)

