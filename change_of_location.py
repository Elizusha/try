import math

def algorithm(hdc, hd, d, h, w, ab, bc, cd):
    f = hd <= 0
    r = math.fabs(hd)
    fi = math.asin(d / r)
    hc = math.sqrt(hd*hd + cd*cd - 2*hd*cd*math.cos(hdc))
    dhc = math.acos((hc*hc + hd*hd - cd*cd)/(2*hc*hd))
    ahc = math.pi/2 - dhc
    ah = w - h
    ac = math.sqrt(ah*ah + hc*hc - 2*ah*hc*math.cos(ahc))
    bac = math.acos((ac * ac + ab * ab - bc * bc) / (2 * ac * ab))
    abc = math.acos((ab * ab + bc * bc - ac * ac) / (2 * bc * ab))
    bca = math.acos((bc * bc + ac * ac - ab * ab) / (2 * bc * ac))
    ach = math.acos((hc * hc + ac * ac - ah * ah) / (2 * hc * ac))
    hcd = math.acos((hc * hc + cd * cd - hd * hd) / (2 * hc * cd))
    acd = ach + hcd
    caf = math.pi - acd - hdc
    a = caf + bac
    b = abc
    c = bca + ach + hcd

    #print('A: ', (a * 180) / math.pi)
    #print('B: ', (b * 180) / math.pi)
    #print('C: ', (c * 180) / math.pi)

    y = 0.0236*hd*hd*hd - 1.7056*hd*hd + 40.3643*hd - 295.5545
    y += -0.0026*hd*hd*hd + 0.2260*hd*hd -6.1877*hd + 55.2097
    #print(y)

    a = 3000 + int(((a * 180) / math.pi - 5) * (6900/ 175))
    b = 3700 + int(((b * 180) / math.pi - 23) * (5600/155))
    c = 3000 + int(((c * 180) / math.pi - 120 + y) * (7000/195))
    #print('A: ', a)
    #print('B: ', b)
    #print('C: ', c)
    if f:
        fi = 5350 + int(((fi * 180) / math.pi) * 43.8)
    else:
        fi = 5350 - int(((fi * 180) / math.pi) * 43.8)

    #print('Fi: ', fi)

    return a, b, c, fi

