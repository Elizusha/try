import maestro
import change_of_location as chol
import math
import time

servo = maestro.Controller('COM5')
up = True

def ch_location_down(first, second, third, base):
    servo.setTarget(0, base)
    time.sleep(2)
    servo.setTarget(3, third)
    time.sleep(5)
    servo.setTarget(2, second)
    time.sleep(2)
    servo.setTarget(1, first)

def ch_location_up(first, second, third, base):
    servo.setTarget(1, first)
    time.sleep(0.25)
    servo.setTarget(2, second)
    time.sleep(0.25)
    servo.setTarget(3, third)
    time.sleep(0.25)
    servo.setTarget(0, base)
    time.sleep(1)

def ch_location_get(first, second, third, base):
    servo.setTarget(0, base)
    servo.setTarget(3, 4000)
    time.sleep(2)
    servo.setTarget(2, 7000)
    time.sleep(1)
    servo.setTarget(1, first + 500)
    time.sleep(2)
    servo.setTarget(2, second)
    time.sleep(2)
    servo.setTarget(3, third)
    time.sleep(2)
    servo.setTarget(1, first)

def ch_location_set(first, second, third, base):
    servo.setTarget(3, 4000)
    time.sleep(2)
    servo.setTarget(2, 7000)
    time.sleep(1)
    servo.setTarget(0, base)
    servo.setTarget(1, first + 500)
    time.sleep(2)
    servo.setTarget(2, second)
    time.sleep(2)
    servo.setTarget(3, third)
    time.sleep(2)
    servo.setTarget(1, first)


def set_speed():
    servo.setSpeed(0, 20)
    servo.setSpeed(1, 50)
    servo.setSpeed(2, 20)
    servo.setSpeed(3, 20)

    servo.setSpeed(1, 60)

    time.sleep(0.2)
    #ch_location(0, 0, 8000, 0)
    #time.sleep(1)
 #   servo.setSpeed(0, 20)
#    ch_location(0, 0, 0, 5000)

def move(r, d, h, p):
    forth = math.asin(10/13)
    #algorithm(adc, r, w, h, d, ab, bc, cd)
    #(hdc, hd, d, h, w, ab, bc, cd)
    servo.setSpeed(0, 10)
    ab = 15.5
    bc = 15.5
    cd = 13
    w = 6.5
    first, second, third, base = chol.algorithm(forth, r, d , h, w, ab, bc, cd)

    if p:
        ch_location_get(first, second, third, base)
    else:
        ch_location_set(first, second, third, base)

    #ch_location(first, 0, 0, base)
    #ch_location(4824, 6940, 5766, 5502)
    time.sleep(2)
    #ch_location(0, 0, 0, 0)
    #ch_location_down(first, second, 0, base)


'''def replace_fig(r1, d1, r2, d2):
    set_speed()
    servo.setTarget(3, 800)
    time.sleep(2)
    move(r1 - 3, d1, 6)
    servo.setTarget(4, 7000)
    time.sleep(2)
    move(r1 - 0.5, d1, 3)
    time.sleep(2)
    servo.setTarget(4, 9000)
    time.sleep(1)
    move(r1, d1, 5)
    time.sleep(2)
    move(r2 - 1, d2, 5)
    time.sleep(2)
    move(r2, d2, 3)
    time.sleep(2)
    servo.setTarget(4, 7000)
    time.sleep(1)
    move(r2, d2, 5)
    servo.setTarget(4, 9000)
    time.sleep(2)
    #servo.close()'''

def replace_fig(r1, d1, r2, d2):
    set_speed()
    h1 = 0.0045 * r1 * r1 * r1 - 0.3416 * r1 * r1 + 8.2893 * r1 - 61.7708
    h2 = - 0.0174 * r1 * r1 + 0.7755 * r1 - 5.6366
    h = (h1 + h2) / 2
    servo.setTarget(4, 7000)
    move(r1, d1, h, True)
    servo.setTarget(4, 9000)
    time.sleep(2)
    h1 = 0.0045 * r2 * r2 * r2 - 0.3416 * r2 * r2 + 8.2893 * r2 - 61.7708
    h2 = - 0.0174 * r2 * r2 + 0.7755 * r2 - 5.6366
    h = (h1 + h2) / 2
    move(r2, d2, h, False)
    servo.setTarget(4, 7000)
    time.sleep(1)
    ch_location_up(7000, 6000, 9000, 5350)
    time.sleep(2)
    servo.setTarget(4, 9000)


if __name__ == '__main__':
    #replace_fig(33.5, 0, 20, 0)
    r1 = math.sqrt(19.1*19.1  + 4.8*4.8)
    r2 = math.sqrt(22.3*22.3 + 8*8)
    ch_location_up(7000, 6000, 9000, 5350)
    replace_fig(r1, 4.8, r2, 8)
    time.sleep(1)
    #
    ch_location_up(7000, 6000, 9000, 5350) #начальная точка
    time.sleep(4)
    #r1 = math.sqrt(25.5 * 25.5 + 4.8 * 4.8)

    # r1 = math.sqrt(22.3 * 22.3 + 8 * 8)
    # r2 = math.sqrt(35.1*35.1 + 1.6*1.6)
    # replace_fig(r1, 8, r2, 1.6)
    # ch_location_up(7000, 6000, 9000, 5350)
    # time.sleep(2)

    # ch_location_up(0, 0, 0, 0)
    # servo.setTarget(3, 800)
    #replace_fig(26.2, 3.986 + 1, 23.779, 7.23+ 1) #координаты зеленой
    #25.75, -2.3, 22.597, 0.946
    #17.18, -5.297
    #replace_fig(25.75, -2.3, 22.597, 0.946)
    #servo.setTarget(4, 9000)
    #ch_location_up(4012, 8343, 0, 5074)
    ch_location_up(0, 0, 0, 0)

    # move(35.1, 1.6, 1, True)
    # time.sleep(2)
    # servo.setTarget(3, 0)
    # time.sleep(3)
    # ch_location_up(0, 0, 0, 0)
