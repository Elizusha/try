class Obj:
    def __init__(self, x = 0, y = 0, color = ''):
        self.x = x
        self.y = y
        self.color = color

    def into(self, other):
        return other[0][0] < self.x < other[1][0] and other[0][1] < self.y < other[1][1]

    def __str__(self):
        pcolor = self.color if self.color else "."
        return "({0:3}:{1:3} {2:2})".format(self.x, self.y, pcolor[0])