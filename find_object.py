import cv2
from object import Object


dict_color = {'blue': Object('blue'),
        'red': Object('red'),
        'green': Object('green'),
        'orange': Object('orange')}

def find_all_color_objects(color_name, img):
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)  # change rgv on hsv
    color = Object(color_name)
    thresh = cv2.inRange(hsv, color.HSVmin, color.HSVmax)  # filter
    # find conturs
    #cv2.imshow('contours', thresh)  # show frame in window
    #cv2.waitKey()
    _, contours, hierarchy = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    # show contur on frame

    list_center = []

    for c in contours:
        #cv2.drawContours(img, [c], -1, (25, 80, 100), 2) #show contour

        M = cv2.moments(c)
        if M["m00"] > 200:
            cX = int((M["m10"] / M["m00"]))
            cY = int((M["m01"] / M["m00"]))
            cv2.circle(img, (cX, cY), 3, dict_color[color_name].Color, -1)
            list_center.append((cX,cY))
    return list_center


def find_center_of_shape(img):
    color = ["blue", "red", "orange", "green"]
    answer = []
    for c in color:
        answer.append((c, find_all_color_objects(c, img)))
    return answer
