from collections import defaultdict
from checkers_algorithm import Algorithm
import cv2
import cv2.aruco as aruco
import time
import threading
from change_servo import replace_fig, ch_location_up
import math

import find_object
from obj_class import Obj

X = 9
Y = 9
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

markers_corners = []
board_corners = []
board_with_objects = []
markers_ids = []
object_centers = []
new_markers = defaultdict(str)
kings = []
felled = []

def find_markers(frame):
    global markers_corners, markers_ids
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    aruco_dict = aruco.Dictionary_get(aruco.DICT_6X6_250)
    parameters = aruco.DetectorParameters_create()
    corners, ids, rejectedImgPoints = aruco.detectMarkers(gray, aruco_dict, parameters=parameters)
    if ids is not None and corners is not None and len(ids) >= 7:
        markers_corners = corners
        markers_ids = ids


def find_board(frame):
    global board_corners
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    board_ret, corners = cv2.findChessboardCorners(gray, (Y, X), None)
    if board_ret:
        board_corners = cv2.cornerSubPix(gray, corners,(11,11),(-1,-1), criteria)
#!!!!!!!!!!!

def draw_image(fr):
    if board_corners is not None and len(board_corners) != 0:
        fr = cv2.drawChessboardCorners(fr, (Y, X), board_corners, True)
    frame = aruco.drawDetectedMarkers(fr, markers_corners)

    cv2.imshow('frame', frame)


def take_values():
    global board_with_objects
    global new_board, new_markers, f_do_move_once
    while (True):
        time.sleep(3)
        lock = threading.Lock()
        with lock:
            new_markers = refactor_markers()
            new_board = refactor_board()
            if new_board:
                board_with_objects = add_objects(new_board)
                find_kings()
                find_felled()
            print_all()
            #print("!!!!", get_step()[0], get_step()[1],  get_step()[2]) #!!!!!!!
            #ch_location_up(0,0,0,0)
            if '1' in new_markers \
                    and '6' in new_markers \
                    and '0' in new_markers \
                    and object_centers[-1] is not None \
                    and board_with_objects is not None \
                    and len(board_with_objects) > 1:
                dy = new_markers['1'][1] - new_markers['2'][1]
                if math.fabs(dy) < 10:
                    print_all()
                    print("goodCALIBRATE: ", dy)
                    communicate()
                else:
                    print("CALIBRATE: ", dy)


def print_all():
    print('MARKERS: ', new_markers)
    print('OBJECT CENTERS: ', object_centers)
    print("BOARD_WITH_OBJECTS: ")
    for e in board_with_objects:
        for s in e:
            print(s, end=' ')
        print('')
    if felled:
        print("FELLED ")
        for e in felled:
            print(e)
    print("KINGS ", kings)


def communicate():
    ch_location_up(8000, 5000, 8500, 5200)
    point1, point2, point3  = get_step()
    print("STEP", point1, point2, point3)
    if point1:
        if point2:
            x1, y1 = get_new_coordinates(point1)
            vec1 = math.sqrt(x1 * x1 + y1 * y1)
            print(point1)
            print("newX1: ", x1, "newY1", y1, "Vec1", vec1)
            x2, y2 = get_new_coordinates(point2)
            vec2 = math.sqrt(x2 * x2 + y2 * y2)
            print(point2)
            print("newX2: ", x2, "newY2", y2, "Vec2", vec2)
            replace_fig(vec1, x1, vec2, x2)
            if point3:
                x1, y1 = get_new_coordinates(point3)
                vec1 = math.sqrt(x1 * x1 + y1 * y1)
                print(point3)
                print("newX3: ", x1, "newY3", y1, "Vec3", vec1)
                point4 = None
                for e in felled:
                    for t in e:
                        if t:
                            point4 = t
                            break
                    if point4:
                        break
                x2, y2 = get_new_coordinates(point4)
                vec2 = math.sqrt(x2 * x2 + y2 * y2)
                print(point4)
                print("newX4: ", x2, "newY4", y2, "Vec4", vec2)
                replace_fig(vec1, x1, vec2 + 1, x2)
        else:
            x1, y1 = get_new_coordinates(point1)
            vec1 = math.sqrt(x1 * x1 + y1 * y1)
            print(point1)
            print("newX1: ", x1, "newY1", y1, "Vec1", vec1)
            point2 = None
            for e in felled:
                for t in e:
                    if t:
                        point2 = t
                        break
                if point2:
                    break
            x2, y2 = get_new_coordinates(point2)
            vec2 = math.sqrt(x2 * x2 + y2 * y2)
            print(point2)
            print("newX2: ", x2, "newY2", y2, "Vec2", vec2)
            replace_fig(vec1, x1, vec2, x2)

            point = kings.pop()
            x1, y1 = get_new_coordinates(point)
            vec1 = math.sqrt(x1 * x1 + y1 * y1)
            print(point)
            print("newX3: ", x1, "newY3", y1, "Vec3", vec1)
            x2, y2 = get_new_coordinates(point3)
            vec2 = math.sqrt(x2 * x2 + y2 * y2)
            print(point3)
            print("newX4: ", x2, "newY4", y2, "Vec4", vec2)
            replace_fig(vec1, x1, vec2, x2)
        ch_location_up(8000, 5000, 8500, 5300)
        input("MAKE STEP AND PRESS ENTER")
    else:
        print("Point not found")

def get_new_coordinates(point):
    marker12_dist = 26.7
    josyX = 13.2
    josyY = 13.7
    dist = math.fabs(new_markers['1'][0] - new_markers['2'][0])
    k = marker12_dist / dist
    josy = new_markers['1']
    x = (point[0] - josy[0])
    y = (point[1] - josy[1])
    x = x * k - josyX #!!!!!!!!!
    y = y * k + josyY
    return x, y


def get_step():
    # if (len(object_centers[-2][1]) != 0):
    #     point1 = object_centers[-2][1][0] #первая желтая фишка
    # else:
    #     point1 = None
    # if (len(object_centers[-1][1]) != 0):
    #     point2 = object_centers[-1][1][0] #первая зеленая фишка
    # else:
    #     point2 = None
    al = Algorithm('green', 'blue', 'orange', 'red')
    print("###")
    print_all()
    point1, point2, point3 = al.get_next_step(board_with_objects)
    return point1, point2, point3


def refactor_markers():
    lock = threading.Lock()
    with lock:
        markers = {}
        for i in range(len(markers_ids)):
            a = markers_corners[i][0][0]
            markers[str(markers_ids[i][0])] = (int(a[0]), int(a[1]))
        return markers


def refactor_board():
    if board_corners is not None and len(board_corners) > 0:
        i = 0
        bc = list(board_corners)
        board = []
        t_board = []
        while i < len(bc):
            d = []
            res = []
            for j in range(X):
                corner = bc[i][0]
                res.append(None)
                d.append((int(corner[0]), int(corner[1])))
                i += 1
            board.append(d)
            t_board.append(d)
        print("Board")
        if math.fabs(board_corners[0][0][0] - board_corners[1][0][0]) < 20:
            print("Board ret")
            for i in range(len(board)):
                for j in range(len(board)):
                    t_board[j][i] = board[i][j]
            board = list(t_board)
        for i in range(len(board) - 1):
            for j in range(len(board[0]) - 1):
                board[i][j] = [board[i][j], board[i + 1][j + 1]]
        for i in range(len(board)):
            board[i].pop()
        board.pop()
        return board


def add_objects(board):
    play = []
    result = []
    for i in range(len(board)):
        a = []
        for j in range(len(board[0])):
            x = math.fabs(board[i][j][1][0] + board[i][j][0][0])/2
            y = math.fabs(board[i][j][1][1] + board[i][j][0][1])/2
            a.append(Obj(int(x), int(y), None))
        result.append(a)
    for e in object_centers:
        for u in e[1]:
            play.append(Obj(u[0], u[1], e[0]))
    for e in play:
        for i in range(len(board)):
            for j in range(len(board[0])):
                if e.into(board[i][j]):
                    result[i][j] = e
    return result


def find_kings():
    global kings
    kings = []
    aruco1 = '3'
    aruco2 = '4'
    k = 30
    if aruco1 in new_markers and aruco2 in new_markers:
        for obj in object_centers[0][1]: #!!!!!kings color
            if new_markers[aruco1][0] - k  < obj[0] < new_markers[aruco2][0] + k and \
                                    new_markers[aruco1][1] - k < obj[1] < new_markers[aruco2][1] + k:
                kings.append(obj)
    return kings


def find_felled():
    global felled
    aruco1 = '5'
    aruco2 = '6'
    k = 30
    px = 2
    py = 6
    felled_board = []
    felled = []
    if aruco1 in new_markers and aruco2 in new_markers:
        dx = math.fabs(new_markers[aruco1][0] - new_markers[aruco2][0])/px
        dy = math.fabs(new_markers[aruco1][1] - new_markers[aruco2][1])/py
        x = new_markers[aruco1][0] - dx/2
        y = new_markers[aruco1][1]
        for i in range(py):
            felled_board.append([])
            felled.append([])
            for j in range(px):
                felled_board[i].append([(x, y),(x + dx, y + dy)])
                felled[i].append((int(x+dx/2), int(y+dy/2)))
                x += 2 * dx
            y += dy
            x = new_markers[aruco1][0] - dx/2
        objects = object_centers[-1][1]
        objects.extend(object_centers[0][1])
        objects.extend(object_centers[1][1])
        objects.extend(object_centers[2][1])
        for obj in objects:
            if new_markers[aruco1][0] - k < obj[0] < new_markers[aruco2][0] + k \
                    and new_markers[aruco1][1] - k < obj[1] < new_markers[aruco2][1] + k:
                for i in range(py):
                    for j in range(px):
                        if felled_board[i][j][0][0] < obj[0] < felled_board[i][j][1][0] \
                                and felled_board[i][j][0][1] < obj[1] < felled_board[i][j][1][1]:
                            felled[i][j] = None
    return felled

def find_object_centres(frame): #очень сомнительный отбор точек!
    global object_centers
    oc = find_object.find_center_of_shape(frame)
    if not(object_centers != [] and len(object_centers[-2][1]) < len(oc[-2][1]) - 2):
        object_centers = oc



def handle():
    global object_centers
    cap = cv2.VideoCapture(1)

    while True:
        ret, frame = cap.read()
        #frame = cv2.imread("green.png")

        find_markers(frame)
        find_board(frame)
        find_object_centres(frame)

        draw_image(frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    #frame.release
    cv2.destroyAllWindows()


def main():
    t1 = threading.Thread(target=handle)
    t2 = threading.Thread(target=take_values)

    t1.start()
    t2.start()

    t1.join()
    t2.join()


if __name__ == '__main__':
    main()



