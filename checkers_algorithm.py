from obj_class import Obj
from enum import Enum


class Priority(Enum):
    hack = 0 # рубить
    kingon = 1 #стать дамкой
    escape = 2 # уйти из под удара - пока нет
    protect = 3 # безопасный ход
    death = 4 # подставится под удар
    no = 5 # некуда идти

    def __lt__(self, other):
        return self.value > other.value


class Algorithm:
    def __init__(self, jozi_col, jozi_king_col, pl_col, pl_king_col):
        self.j_col = jozi_col
        self.j_k_col = jozi_king_col
        self.p_col = pl_col
        self.p_k_col = pl_king_col

    def get_next_step(self, board):
        next_step = (Priority.no, None, (None, None), None)
        for i in range(len(board)):
            for j in range(len(board[0])):
                if self.its_your(board[i][j].color):
                    now = self.evaluate_next_step(board, i, j)
                    if next_step[0] < now[0]:
                        next_step = (now[0], (i, j), now[1], now[2])

        if next_step[1] == None:
            return (None, None, None)

        start = board[next_step[1][0]][next_step[1][1]]
        end = board[next_step[2][0]][next_step[2][1]]

        if next_step[0] == Priority.kingon:
            return ((start.x, start.y), None, (end.x, end.y))

        hack = None
        if next_step[3] != None:
            h = board[next_step[3][0]][next_step[3][1]]
            hack = (h.x, h.y)
        return ((start.x, start.y), (end.x, end.y), hack)

    def evaluate_next_step(self, board, i, j):
        king_step = None  # return (end point, hack point)
        if (board[i][j].color == self.j_k_col):
            king_step = self.king_make_step(board, i, j)

        if king_step != None:
            return (Priority.hack, king_step[0], king_step[1])

        #print(i,j)
        back = self.hack_back(board, i, j)
        if (back != None):
            return back

        in_left = self.get_priority_through_one_cage(board, i, j, -1)
        in_right = self.get_priority_through_one_cage(board, i, j, 1)

        #see(board)

        if in_right > in_left:
            end_point = self.define_endpoint(1, i, j, in_right)
            return (in_right, end_point[0], end_point[1])
        end_point = self.define_endpoint(-1, i, j, in_left)
        return (in_left, end_point[0], end_point[1])

    def hack_back(self, board, i, j):
        if i-2 < 0: return None
        if (j-2 >= 0):
            if self.its_enemy(board[i-1][j-1].color) and board[i-2][j-2].color == None:
                return (Priority.hack, (i-2, j-2), (i-1, j-1))
        if j+2 < len(board[0]):
            if self.its_enemy(board[i-1][j+1].color) and board[i-2][j+2].color == None:
                return (Priority.hack, (i-2, j+2), (i-1, j+1))
        return None

    def define_endpoint(self, dir, i, j, priority): #возвращает ((кооорд. конца хода),(коор. рубленой фишки или дамка))
        if priority == Priority.protect or priority == Priority.death: return ((i + 1, j + dir), None)
        if priority == Priority.hack: return ((i + 2, j + 2 * dir), (i+1, j+1*dir))
        if priority == Priority.kingon: return ((i + 1, j + dir), (i + 1, j + dir))
        return ((i, j), None)

    def its_your(self, color):
        return color == self.j_col or color == self.j_k_col

    def its_enemy(self, color):
        return color == self.p_col or color == self.p_k_col

    def get_priority_through_one_cage(self, board, i, j, dir):
        if (i+1 >= len(board)): return Priority.no
        len_y = len(board[0])
        if (j + dir < 0 or j + dir > len_y - 1): return Priority.no
        if (i + 1 == len(board)-1):
            if board[i+1][j+dir].color == None:
                return Priority.kingon
            return Priority.no
        if (j + dir == 0 or j + dir == len_y-1):
            if board[i+1][j+dir].color == None:
                return Priority.protect
            return Priority.no
        #print(i,j)
        if self.its_your(board[i+1][j+dir].color):
            return Priority.no
        if (board[i+1][j+dir].color != None
            and board[i+2][j+2*dir].color != None):
            return Priority.no
        if (self.its_enemy(board[i+1][j+dir].color)
            and board[i+2][j+2*dir].color == None):
            return Priority.hack
        if (board[i+1][j+dir].color == None
            and self.its_enemy(board[i+2][j+2*dir].color)):
            return Priority.death
        if board[i+1][j+dir].color == None:
            if (self.its_enemy(board[i + 2][j].color)
                and board[i][j + 2 * dir].color == None):
                return Priority.death
            if (self.its_enemy(board[i][j + 2 * dir].color)
                and board[i + 2][j].color == None):
                return Priority.death
        return Priority.protect

    def king_make_step(self, board, i, j):
        left_up = self.check_diagonal(-1, -1, i, j, board)
        if left_up != None: return left_up
        left_down = self.check_diagonal(1, -1, i, j, board)
        if left_down != None: return left_down
        right_up = self.check_diagonal(-1, 1, i, j, board)
        if right_up != None: return right_up
        right_down = self.check_diagonal(1, 1, i, j, board)
        if right_down != None: return right_down
        return None


    def check_diagonal(self, dir_x, dir_y, i, j, board):
        len_x = len(board)
        len_y = len(board[0])
        i_now = i +dir_x #
        j_now = j +dir_y
        while (i_now >= 0 and j_now >= 0 and i_now<len_x and j_now< len_y):
            if self.its_enemy(board[i_now][j_now].color):
                if (i_now +dir_x < 0 or j_now +dir_y < 0
                    or i_now +dir_x >= len_x or j_now +dir_y >=len_y): return None
                if board[i_now +dir_x][j_now +dir_y].color == None:
                    return ((i_now +dir_x, j_now +dir_y), (i_now, j_now))
            if self.its_your(board[i_now][j_now].color): break
            i_now += dir_x
            j_now += dir_y
        return None






def main():
    al = Algorithm('g', 'b', 'e', 'r')
    board = []
    for i in range(8):
        st = []
        for j in range(8):
            st.append(Obj(i*10+5, j*10+5))
        board.append(st)

    board[5][4].color = 'g'
    board[4][3].color = 'e'
    see(board)
    print( al.get_next_step(board))

def see(board):
    for line in board:
        l = []
        for val in line:
            l.append('{}:{} {}   '.format(val.x if val.x // 10 !=0 else ' '+ str(val.x),
                                          val.y, val.color if val.color !=None else ' '))
        #print(l)
