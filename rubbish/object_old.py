import cv2
import numpy as np

class Object:
    def __init__(self, name = "Object"):
        self.xPos = -1
        self.yPos = -1
        self.type = name
        if name == "Object":
            self.Color = (0, 0, 0)
            return
        if name == "blue":
            self.HSVmin = np.array((119,29,82), np.uint8)
            self.HSVmax = np.array((121,255,255), np.uint8)
            self.Color = (255,0,0)
        if name == "green":
            self.HSVmin = np.array((49,7,154), np.uint8)
            self.HSVmax = np.array((100,255,255), np.uint8)
            self.Color = (0,255,0)
        if name == "orange":
            self.HSVmin = np.array((13,23,235), np.uint8)
            self.HSVmax = np.array((21,255,255), np.uint8)
            self.Color = (0,255,255)
        if name == "red":
            self.HSVmin = np.array((0,23,237), np.uint8)
            self.HSVmax = np.array((0,255,255), np.uint8)
            self.Color = (0,0,255)

