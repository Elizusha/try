import cv2
import numpy as np

if __name__ == '__main__':
    def nothing(*arg):
        pass

cv2.namedWindow("result")
cv2.namedWindow("settings")


cv2.createTrackbar('color start', 'settings', 0, 255, nothing)
cv2.createTrackbar('saturation start', 'settings', 0, 255, nothing)
cv2.createTrackbar('bring start', 'settings', 0, 255, nothing)
cv2.createTrackbar('color end', 'settings', 255, 255, nothing)
cv2.createTrackbar('saturation end', 'settings', 255, 255, nothing)
cv2.createTrackbar('bring end', 'settings', 255, 255, nothing)
crange = [0, 0, 0, 0, 0, 0]


img = cv2.imread("lol.jpg")


while True:
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

    h1 = cv2.getTrackbarPos('color start', 'settings')
    s1 = cv2.getTrackbarPos('saturation start', 'settings')
    v1 = cv2.getTrackbarPos('bring start', 'settings')
    h2 = cv2.getTrackbarPos('color end', 'settings')
    s2 = cv2.getTrackbarPos('saturation end', 'settings')
    v2 = cv2.getTrackbarPos('bring end', 'settings')

    h_min = np.array((h1, s1, v1), np.uint8)
    h_max = np.array((h2, s2, v2), np.uint8)

    thresh = cv2.inRange(hsv, h_min, h_max)

    cv2.imshow('result', thresh)

    ch = cv2.waitKey(5)
    if ch == 0:
        break



cv2.destroyAllWindows()