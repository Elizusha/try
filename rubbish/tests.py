from checkers_algorithm import Algorithm
from obj_class import Obj
import unittest


class TestStatistics(unittest.TestCase):

    def test_hack_left(self):
        al = Algorithm('g', 'b', 'e', 'r')
        board = []
        for i in range(8):
            st = []
            for j in range(8):
                st.append(Obj(i * 10 + 5, j * 10 + 5))
            board.append(st)

        board[5][4].color = 'g'
        board[4][3].color = 'e'
        self.assertEqual(al.get_next_step(board), ((55, 45), (35, 25)))

    def test_hack_right(self):
        al = Algorithm('g', 'b', 'e', 'r')
        board = []
        for i in range(8):
            st = []
            for j in range(8):
                st.append(Obj(i * 10 + 5, j * 10 + 5))
            board.append(st)

        board[5][4].color = 'g'
        board[4][5].color = 'e'
        self.assertEqual(al.get_next_step(board), ((55, 45), (35, 65)))

    def test_hack_back_right(self):
        al = Algorithm('g', 'b', 'e', 'r')
        board = []
        for i in range(8):
            st = []
            for j in range(8):
                st.append(Obj(i * 10 + 5, j * 10 + 5))
            board.append(st)

        board[5][4].color = 'g'
        board[6][5].color = 'e'
        self.assertEqual(al.get_next_step(board), ((55, 45), (75, 65)))


    def test_hack_back_left(self):
        al = Algorithm('g', 'b', 'e', 'r')
        board = []
        for i in range(8):
            st = []
            for j in range(8):
                st.append(Obj(i * 10 + 5, j * 10 + 5))
            board.append(st)

        board[5][4].color = 'g'
        board[6][3].color = 'e'
        self.assertEqual(al.get_next_step(board), ((55, 45), (75, 25)))



if __name__ == '__main__':
    unittest.main()
