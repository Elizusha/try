import time
import threading
from obj_class import Obj

def print_data(markers_corners, markers_ids, board_corners, object_centers, X):
    while (True):
        time.sleep(5)
        board = []
        markers = []
        lock = threading.Lock()
        with lock:
            for i in range(len(markers_ids)):
                a = markers_corners[i][0][0]
                markers.append([markers_ids[i][0], (int(a[0]), int(a[1]))])
            if board_corners is not None and len(board_corners) > 0:
                i = 0
                bc = list(board_corners)
                bc.reverse()
                result = []
                while i < len(bc):
                    d = []
                    res = []
                    for j in range(X):
                        corner = bc[i][0]
                        res.append(None)
                        d.append((int(corner[0]), int(corner[1])))
                        i += 1
                    board.append(d)
                    result.append(res)
                for i in range(len(board) - 1):
                    for j in range(len(board[0]) - 1):
                        board[i][j] = [board[i][j], board[i + 1][j + 1]]
                for i in range(len(board)):
                    board[i].pop()
                    result[i].pop()
                board.pop()
                result.pop()
                play = []
                for e in object_centers:
                    for u in e[1]:
                        play.append(Obj(u[0], u[1], e[0]))
                for e in play:
                    for i in range(len(board)):
                        for j in range(len(board[0])):
                            if e.into(board[i][j]):
                                result[i][j] = e
                for e in result:
                    print(e)
        print(markers)
        print(object_centers)
        print('')